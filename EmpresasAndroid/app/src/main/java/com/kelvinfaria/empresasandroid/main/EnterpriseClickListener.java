package com.kelvinfaria.empresasandroid.main;

import android.view.View;

import com.kelvinfaria.empresasandroid.model.EnterpriseModel;

// Define uma interface a implementar pela classe que
// ira executar o código quando for clicado um item enterprise
public interface EnterpriseClickListener
{
    void itemClicked(View view, EnterpriseModel enterpriseModel);
}
