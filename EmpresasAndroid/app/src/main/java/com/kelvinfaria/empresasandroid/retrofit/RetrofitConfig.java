package com.kelvinfaria.empresasandroid.retrofit;

import com.kelvinfaria.empresasandroid.service.EnterpriseService;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitConfig
{
    private final Retrofit retrofit;

    //Configuracao default do retrofit
    public RetrofitConfig()
    {
        this.retrofit = new Retrofit.Builder()
                .baseUrl("http://empresas.ioasys.com.br/api/v1/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public EnterpriseService getApiEnterprise()
    {
      return this.retrofit.create(EnterpriseService.class);
    }
}
