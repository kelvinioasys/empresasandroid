package com.kelvinfaria.empresasandroid.service;

import com.google.gson.JsonObject;
import com.kelvinfaria.empresasandroid.model.LoginModel;


import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface EnterpriseService
{
    //EndPoint de login passando os parametros
    @POST("users/auth/sign_in")
    @FormUrlEncoded
    Call<LoginModel> sign_in(@Field("email") String email,
                             @Field("password") String password);

    //EndPoint para o recebimento da lista de enterprises passando os parametros
    @GET("enterprises/")
    Call<JsonObject> consultEnterprise(@Query("access-token") String access_token,
                                       @Query("client") String client,
                                       @Query("uid") String uid);

}
