package com.kelvinfaria.empresasandroid.adapter;

import android.content.Context;
import android.support.v4.widget.CircularProgressDrawable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.kelvinfaria.empresasandroid.R;
import com.kelvinfaria.empresasandroid.main.EnterpriseClickListener;
import com.kelvinfaria.empresasandroid.model.EnterpriseModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class EnterpriseAdapter extends RecyclerView.Adapter<EnterpriseAdapter.ItemViewHolder> implements Filterable
{
    private static List<EnterpriseModel> enterpriseList;// Lista temporaria
    private static List<EnterpriseModel> enterpriseListFull;// Lista completa
    private boolean enterpriseListTemp = false;
    private LayoutInflater mInflater;
    private Context context;
    private EnterpriseClickListener clicklistener = null;// Adiciona ao adapter um atributo e o respectivo setter para guardar uma instancia que implemente essa interface

    //Instanciando o adapter
    public EnterpriseAdapter(Context ctx, List<EnterpriseModel> enterprise)
    {
        context = ctx;
        enterpriseList = enterprise;
        enterpriseListFull = enterpriseList;
        mInflater = LayoutInflater.from(context);
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        //Declarando elementos de layout
        public ImageView imageEnterpriseList;
        public TextView textEnterpriseName;
        public TextView textEnterpriseType;
        public TextView textEnterpriseCountry;
        public CardView cardViewDetails;

        public ItemViewHolder(View itemView)
        {
            super(itemView);

            //Instanciando os elementos de layout
            imageEnterpriseList = itemView.findViewById(R.id.imageEnterpriseList);
            textEnterpriseName = itemView.findViewById(R.id.textEnterpriseName);
            textEnterpriseType = itemView.findViewById(R.id.textEnterpriseType);
            textEnterpriseCountry = itemView.findViewById(R.id.textEnterpriseCountry);
            cardViewDetails = itemView.findViewById(R.id.cardViewDescription);
            cardViewDetails.setOnClickListener(this);// Atribui o listener ao click no card
        }

        //Implementa View.OnClickListener
        @Override
        public void onClick(View v)
        {
            if (clicklistener != null)
            {
                clicklistener.itemClicked(v, enterpriseList.get(getAdapterPosition()));
            }

        }
    }

    public void setClickListener(EnterpriseClickListener listener)
    {
        this.clicklistener = listener;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_enterprise, parent, false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, final int position)
    {

        CircularProgressDrawable progressDrawableList = new CircularProgressDrawable(context);
        progressDrawableList.setStrokeWidth(5f);
        progressDrawableList.setCenterRadius(30f);
        progressDrawableList.start();

        //Populando elementos de layout
        Glide.with(context)
                .load(context.getString(R.string.BASE_URL) + enterpriseList.get(position).getPhoto())
                .fitCenter()
                .placeholder(progressDrawableList)
                .error(R.drawable.img_e_1_lista)
                .into(holder.imageEnterpriseList);
        holder.textEnterpriseName.setText(enterpriseList.get(position).getEnterprise_name());
        holder.textEnterpriseType.setText(enterpriseList.get(position).getEnterprise_type().getEnterprise_type_name());
        holder.textEnterpriseCountry.setText(enterpriseList.get(position).getCountry());
    }

    @Override
    public int getItemCount()
    {
        return enterpriseList.size();
    }

    //Implementacao para filtro de pesquisa
    @Override
    public Filter getFilter()
    {
        Filter filter = new Filter()
        {
            @Override
            protected FilterResults performFiltering(CharSequence constraint)
            {
                ArrayList<EnterpriseModel> FilteredArrayNames = new ArrayList<>();

                if (constraint == null || constraint.length() == 0)
                {
                    FilteredArrayNames.addAll(enterpriseListFull);
                }
                else
                {
                    constraint = constraint.toString().toLowerCase().trim();

                    for (EnterpriseModel dataNames : enterpriseListFull)
                    {
                        if (dataNames.getEnterprise_name().toLowerCase().startsWith(constraint.toString()))
                        {
                            FilteredArrayNames.add(dataNames);
                        }
                    }
                }

                FilterResults results = new FilterResults();
                results.values = FilteredArrayNames;


                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results)
            {
                if (enterpriseListTemp)
                {
                    enterpriseList = enterpriseListFull;
                    notifyDataSetChanged();
                    enterpriseListTemp = false;
                }
                else
                {
                    enterpriseList = (List<EnterpriseModel>) results.values;
                    notifyDataSetChanged();
                }
            }
        };

        return filter;
    }
}
