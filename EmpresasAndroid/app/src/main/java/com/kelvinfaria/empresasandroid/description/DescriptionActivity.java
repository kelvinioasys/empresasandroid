package com.kelvinfaria.empresasandroid.description;

import android.content.Intent;
import android.support.v4.widget.CircularProgressDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.kelvinfaria.empresasandroid.R;
import com.kelvinfaria.empresasandroid.model.EnterpriseModel;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DescriptionActivity extends AppCompatActivity
{
    //Elementos de layout
    @BindView(R.id.imageEnterprise) ImageView imageEnterprise;
    @BindView(R.id.textDescription) TextView textDescription;
    @BindView(R.id.toolbarDescription) Toolbar toolbarDescription;
    @BindView(R.id.textEnterpriseName) TextView textEnterpriseName;

    EnterpriseModel mEnterpriseModel;
    Gson mGson;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description);
        ButterKnife.bind(this);

        mGson = new Gson();

        //Recebendo os parametros da view anterior
        //e atribuindo os valores no objeto mEnterpriseModel
        try
        {
            Intent intent = getIntent();
            Bundle b = intent.getExtras();

            assert b != null;
            mEnterpriseModel = mGson.fromJson(b.getSerializable("mEnterpriseList").toString(), EnterpriseModel.class);

            fillFields();
        }
        catch (Exception e )
        {
            e.printStackTrace();
        }

        descriptionToolbarConfig();
    }

    //Configuracao da toolbar customizada
    private void descriptionToolbarConfig()
    {
        setSupportActionBar(toolbarDescription);

        toolbarDescription.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbarDescription.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
            }

        });

        textEnterpriseName.setText(mEnterpriseModel.getEnterprise_name());
    }

    //Preenchimento dos dados da tela
    private void fillFields()
    {
        CircularProgressDrawable progressDrawableList = new CircularProgressDrawable(this);
        progressDrawableList.setStrokeWidth(5f);
        progressDrawableList.setCenterRadius(30f);
        progressDrawableList.start();

        //Populando elementos de layout
        Glide.with(this)
                .load(this.getString(R.string.BASE_URL) + mEnterpriseModel.getPhoto())
                .fitCenter()
                .placeholder(progressDrawableList)
                .error(R.drawable.img_e_1)
                .into(imageEnterprise);

        textDescription.setText(mEnterpriseModel.getDescription());
    }
}
