package com.kelvinfaria.empresasandroid.main;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.kelvinfaria.empresasandroid.R;
import com.kelvinfaria.empresasandroid.adapter.EnterpriseAdapter;
import com.kelvinfaria.empresasandroid.description.DescriptionActivity;
import com.kelvinfaria.empresasandroid.model.ConfigurationModel;
import com.kelvinfaria.empresasandroid.model.EnterpriseModel;
import com.kelvinfaria.empresasandroid.retrofit.RetrofitConfig;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity implements EnterpriseClickListener
{
    //Elementos de layout
    @BindView(R.id.toolbarSearch) Toolbar toolbarSearch;
    @BindView(R.id.searchView) SearchView searchView;
    @BindView(R.id.logoToolbar) ImageView logoToolbar;
    @BindView(R.id.textClique) TextView textClique;
    @BindView(R.id.linearRecyclerView) LinearLayout linearRecyclerView;
    @BindView(R.id.progressBarSearch) ProgressBar mProgressBarSearch;

    private RecyclerView mRecycleView;
    private EnterpriseAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    ConfigurationModel mConfigurationModel;
    Gson mGson;
    List<EnterpriseModel> mEnterpriseList;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mConfigurationModel = new ConfigurationModel();
        mGson = new Gson();

        //Recebendo os parametros da view anterior
        //e atribuindo os valores no objeto mConfigurationModel
        try
        {
            Intent intent = getIntent();
            Bundle b = intent.getExtras();

            assert b != null;
            mConfigurationModel = mGson.fromJson(b.getSerializable("mConfigurationModel").toString(), ConfigurationModel.class);

        }
        catch (Exception e )
        {
            e.printStackTrace();
        }

        mainToolbarConfig();

        //Implementacao do searchview, pegando os caracteres digitados
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s)
            {
                MainActivity.this.mAdapter.getFilter().filter(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s)
            {
                MainActivity.this.mAdapter.getFilter().filter(s);
                return true;
            }

        });
    }

    //Configuracao da toolbar customizada
    private void mainToolbarConfig()
    {
        setSupportActionBar(toolbarSearch);

        searchView.setOnSearchClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                mProgressBarSearch.setVisibility(View.VISIBLE);

                searchView.setMaxWidth(Integer.MAX_VALUE);
                logoToolbar.setVisibility(View.GONE);

                consultEnterprise();

                textClique.setVisibility(View.GONE);
                linearRecyclerView.setVisibility(View.VISIBLE);

            }
        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener()
        {
            @Override
            public boolean onClose()
            {
                mProgressBarSearch.setVisibility(View.GONE);

                logoToolbar.setVisibility(View.VISIBLE);
                textClique.setVisibility(View.VISIBLE);
                linearRecyclerView.setVisibility(View.GONE);

                return false;
            }
        });
    }


    //Faz a requisicao no servidor
    private void consultEnterprise()
    {
        Call<JsonObject> call = new RetrofitConfig().getApiEnterprise().consultEnterprise(
                mConfigurationModel.getAccessToken(),
                mConfigurationModel.getClient(),
                mConfigurationModel.getUid());
        call.enqueue(new Callback<JsonObject>()
        {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response)
            {
                mProgressBarSearch.setVisibility(View.GONE);

                //Valida se teve sucesso na requisicao
                if (response.isSuccessful())
                {
                    //Recebendo lista de empresas e populando o adapter
                    mEnterpriseList = Arrays.asList(mGson.fromJson(response.body().get("enterprises"), EnterpriseModel[].class));
                    mRecycleView = findViewById(R.id.recyclerView);
                    mLayoutManager = new LinearLayoutManager(MainActivity.this);

                    mAdapter = new EnterpriseAdapter(MainActivity.this, mEnterpriseList);

                    mRecycleView.setLayoutManager(mLayoutManager);
                    mRecycleView.setAdapter(mAdapter);
                    mAdapter.setClickListener(MainActivity.this);
                }
                else
                {
                    Toast.makeText(MainActivity.this, "Ocorreu uma falha.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t)
            {
                //failure
                mProgressBarSearch.setVisibility(View.GONE);
            }
        });
    }

    //Sera passada uma view que foi clicada, a posicao da view e o objeto que esta contido em sua lista na sua respectiva posicao.
    //Agora implementamos a interface.
    @Override
    public void itemClicked(View view, EnterpriseModel enterpriseModel)
    {
        Intent intent = new Intent(MainActivity.this, DescriptionActivity.class);
        intent.putExtra("mEnterpriseList",mGson.toJson(enterpriseModel));// Passando um objeto da lista para a DescriptionActivity
        startActivity(intent);
    }

}
