package com.kelvinfaria.empresasandroid.login;

import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.kelvinfaria.empresasandroid.R;
import com.kelvinfaria.empresasandroid.main.MainActivity;
import com.kelvinfaria.empresasandroid.model.ConfigurationModel;
import com.kelvinfaria.empresasandroid.model.LoginModel;
import com.kelvinfaria.empresasandroid.retrofit.RetrofitConfig;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity
{
    //Elementos de layout
    @BindView(R.id.textEmail) EditText textEmail;
    @BindView(R.id.textInputEmail) TextInputLayout hintEmail;
    @BindView(R.id.textPassword) EditText textPassword;
    @BindView(R.id.textInputPassword) TextInputLayout hintPassword;
    @BindView(R.id.progressBar) ProgressBar mProgressBar;
    @BindView(R.id.buttonEntrar) Button buttonEntrar;

    View focusView = null;

    ConfigurationModel mConfigurationModel;
    Gson mGson;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        mConfigurationModel = new ConfigurationModel();
        mGson = new Gson();

        textEmail.setText("testeapple@ioasys.com.br");
        textPassword.setText("12341234");
    }

    //Validacao dos campos digitaveis da tela
    private boolean validateFields()
    {
        if (TextUtils.isEmpty(textEmail.getText().toString()))
        {
            hintEmail.setError("Informe seu e-mail");
            focusView = textEmail;
            return false;
        }

        if (!textEmail.getText().toString().contains("@") || !textEmail.getText().toString().contains("."))
        {
            hintEmail.setError("Informe um e-mail valido");
            focusView = textEmail;
            return false;
        }

        if (TextUtils.isEmpty(textPassword.getText().toString()))
        {
            hintPassword.setError("Informe sua senha");
            focusView = textPassword;
            return false;
        }
        return true;
    }

    //Clique do botao para entrar (implementacao do ButterKnife)
    @OnClick(R.id.buttonEntrar) public void buttonEntrar()
    {
        if (validateFields())
        {
            mProgressBar.setVisibility(View.VISIBLE);
            buttonEntrar.setEnabled(false);
            login();
        }
    }

    //Faz a requisicao no servidor
    private void login()
    {
        LoginModel loginModel = new LoginModel();
        loginModel.setEmail(textEmail.getText().toString().trim());
        loginModel.setPassword(textPassword.getText().toString());

        Call<LoginModel> call = new RetrofitConfig().getApiEnterprise().sign_in(loginModel.getEmail(), loginModel.getPassword());
        call.enqueue(new Callback<LoginModel>()
        {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response)
            {
                mProgressBar.setVisibility(View.GONE);

                //Valida se teve sucesso na requisicao
                if (response.isSuccessful())
                {
                    //Campos necessarios para as proximas requisicoes
                    mConfigurationModel.setAccessToken(response.headers().get("access-token"));
                    mConfigurationModel.setClient(response.headers().get("client"));
                    mConfigurationModel.setUid(response.headers().get("uid"));

                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    intent.putExtra("mConfigurationModel",mGson.toJson(mConfigurationModel));//Passando os parametro para as proximas requisicoes
                    startActivity(intent);
                }
                else
                {
                    Toast.makeText(LoginActivity.this, "Usuário ou senha inválidos.", Toast.LENGTH_SHORT).show();
                    buttonEntrar.setEnabled(true);
                }
            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable t)
            {
                //failure
                mProgressBar.setVisibility(View.GONE);
                buttonEntrar.setEnabled(true);
            }
        });
    }

    @Override
    protected void onResume()
    {
        super.onResume();

//        textEmail.setText("");
//        textPassword.setText("");

        buttonEntrar.setEnabled(true);
    }
}




















