package com.kelvinfaria.empresasandroid.model;

public class EnterpriseModel
{

    private float id;
    private String enterprise_name;
    private String description;
    private String email_enterprise;
    private String facebook;
    private String twitter;
    private String linkedin;
    private String phone;
    private boolean own_enterprise;
    private String photo;
    private float value;
    private float shares;
    private float share_price;
    private float own_shares;
    private String city;
    private String country;
    private EnterpriseTypeModel enterprise_type;

    public EnterpriseModel(String enterprise_name, String description, String photo, String country, EnterpriseTypeModel enterprise_type)
    {
        this.enterprise_name = enterprise_name;
        this.description = description;
        this.photo = photo;
        this.country = country;
        this.enterprise_type = enterprise_type;
    }

    public float getId() {
        return id;
    }

    public void setId(float id) {
        this.id = id;
    }

    public String getEnterprise_name() {
        return enterprise_name;
    }

    public void setEnterprise_name(String enterprise_name) {
        this.enterprise_name = enterprise_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmail_enterprise() {
        return email_enterprise;
    }

    public void setEmail_enterprise(String email_enterprise) {
        this.email_enterprise = email_enterprise;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getLinkedin() {
        return linkedin;
    }

    public void setLinkedin(String linkedin) {
        this.linkedin = linkedin;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isOwn_enterprise() {
        return own_enterprise;
    }

    public void setOwn_enterprise(boolean own_enterprise) {
        this.own_enterprise = own_enterprise;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public float getShares() {
        return shares;
    }

    public void setShares(float shares) {
        this.shares = shares;
    }

    public float getShare_price() {
        return share_price;
    }

    public void setShare_price(float share_price) {
        this.share_price = share_price;
    }

    public float getOwn_shares() {
        return own_shares;
    }

    public void setOwn_shares(float own_shares) {
        this.own_shares = own_shares;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public EnterpriseTypeModel getEnterprise_type() {
        return enterprise_type;
    }

    public void setEnterprise_type(EnterpriseTypeModel enterprise_type) {
        this.enterprise_type = enterprise_type;
    }
}
