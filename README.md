﻿![N|Solid](logo_ioasys.png)

# README #

Kelvin Eimar de Faria Fernandes

contato.kelvinfaria@gmail.com
www.linkedin.com/in/kelvinfaria

### Justificativas de Cada Biblioteca. ###

* Glide Library 
    implementation 'com.github.bumptech.glide:glide:3.7.0'

  Glide é uma biblioteca para gerenciamento de midia como videos, imagens e gifs.
  Ela foi utilizada para o tratamento das imagens das empresas que são buscadas para exibição no app.

* ButterKnife Library 
    implementation 'com.jakewharton:butterknife:8.8.1'
    annotationProcessor 'com.jakewharton:butterknife-compiler:8.8.1'
  
  ButterKnife é uma biblioteca previne linhas de codigo repetitivas como `findViewById(R.id.yourview)`.


* Retrofit Library
    implementation 'com.squareup.retrofit2:retrofit:2.4.0'
    implementation 'com.squareup.retrofit2:converter-gson:2.4.0'
  
  Retrofit é uma biblioteca que transforma uma API HTTP em uma interface java.

* Gson Library
    implementation 'com.google.code.gson:gson:2.8.5'
 
  Gson é a biblioteca que converte objetos java em suas representações JSON e tambem serve para converter
  um string JSON para seu objeto java equivalente. 


### O que faria se tivesse mais tempo. ###

* Corrigiria o bug das imagens esticadas na lista e na descrição das empresas.
  Assim que a lista é carregada as imagens ficam esticadas, porem apos acessar alguma tela de descrição ou mover para baixo e para cima na lista
  as imagens corrigem de tamanho. Ao acessar e sair da tela de descrição as imagens da descriçao tambem se corrigem.

* Teria deixado a SearchBar mais parecida com o modelo solicitado.
  Faltou colocar o icone da lupa branca fora do campo da pesquisa e deixar a linha de baixo do texto na cor branca.

* Teria feito um filtro de pesquisa mais dinamico.
  O filtro atual so responde quando a input de caracteres, quando caracteres são apagados a pesquisa não funciona.

* Colocaria um ProgressBar enquanto a lista esta sendo carregada.

* Corrigiria o bug do clique no card das empresas.
  Ao clicar varias vezes em cima de um card são abertas varias activities repetidas.  
  

### Instruções de como utilizar a aplicação. ###

* Após instalar a aplicação, clique no icone para executa-la.

* É necessario possuir um email e senha cadastrados para acessar a lista de empresas.
  Para testes podem ser usar usados os seguintes dados:
* Usuário de Teste: testeapple@ioasys.com.br
* Senha de Teste : 12341234

* Ao fazer o login clique na lupa no canto superior direito para iniciar o busca.

* Será exibida uma lista de empresas que pode ser filtrada pelo campo de busca

* Ao clicar em alguma empresa vocês sera redirecionado para a tela de descrição da empresa.

* Clicando no botão de voltar no canto superior esquerdo da tela de descrição da empresa,
  você retornara a tela de principal do app onde poderá realizar uma nova busca.
